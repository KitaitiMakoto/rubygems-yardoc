rubygems-yardoc
===============

This gem provides `gem yardoc` command which generates YARD documentations for specified gems.

Installation
------------

    $ gem install rubygems-yardoc

Usage
-----

    $ gem yardoc GEMNAME [GEMNAME ...]

Makes `yard` directory into specified gem's doc directory and
YARD Documentation for the gem will be generated into the directory.

Contributing
------------

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
